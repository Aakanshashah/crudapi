document.getElementById('getApi').addEventListener('click', getApi);
document.getElementById('createPostApi').addEventListener('click', createPostApi);
document.getElementById('createPutApi').addEventListener('click', createPutApi);
document.getElementById('createDeleteApi').addEventListener('click', createDeleteApi);



function getApi() {
    fetch('https://jsonplaceholder.typicode.com/users')
        .then(function(res) {
            return res.json();
        })
        .then(function(data) {

            let html = '<h2 class="text-center ">USER DETAILS</h2> <hr>';
            data.forEach(function(item) {
                html += `    
                <h6 class="text-center fw-bold text-capitalize fs-4" >User Id - ${item.id}</h6>
                <p class="text-center text-capitalize"> <b>Name</b> :${item.name}</p> 
                <p class="text-center text-capitalize"> <b>User Name</b>: ${item.username}</p> 
                <p class="text-center text-capitalize"> <b>User Email </b>: ${item.email}</p>
                <p class="text-center text-capitalize"> <b> User address(street) </b>: ${item.address.street}</p>
                <p class="text-center text-capitalize"> <b> User address(suite)</b>: ${item.address.suite}</p>
                <p class="text-center text-capitalize"> <b> User address(city) </b>: ${item.address.city}</p>
                <p class="text-center text-capitalize"> <b> User address(zipcode) </b>: ${item.address.zipcode}</p>
                <p class="text-center text-capitalize"> <b> User address(geo lat) </b>: ${item.address.geo.lat}</p>
                <p class="text-center text-capitalize"> <b> User address(geo lng) </b> : ${item.address.geo.lng}</p>
                <p class="text-center text-capitalize"> <b> User phoneno </b> : ${item.phone}</p>
                <p class="text-center text-capitalize"> <b> User website </b> : ${item.website}</p>
                <p class="text-center text-capitalize"> <b> User Company name </b> : ${item.company.name}</p>
                <p class="text-center text-capitalize"> <b> User company catchPhrase </b> : ${item.company.catchPhrase}</p>
                <p class="text-center text-capitalize"> <b> User company bs </b>: ${item.company.bs}</p>
                <hr>`
            })

            document.getElementById('container').innerHTML = html;
        })


}

function createPostApi() {
    let html = '<h2 class="text-center "> NEW USER DETAILS</h2> <hr>';
    html += `
    
    <p class="text-center text-capitalize"> <b>Name *</b> :  <input type="text" id="name" ></p> 
    <p class="text-center text-capitalize"> <b>User Name *</b>:  <input type=text id="username"></p> 
    <p class="text-center text-capitalize"> <b>User Email *</b>:  <input type=text id="email" ></p>
    <p class="text-center text-capitalize"> <b> User address *(street) </b>:<input type=text id="street"></p>
    <p class="text-center text-capitalize"> <b> User address(suite)</b>: <input type=text id="suite"></p>
    <p class="text-center text-capitalize"> <b> User address(city) * </b>:<input type=text id="city"></p>
    <p class="text-center text-capitalize"> <b> User address(zipcode) * </b>: <input type=text id="zipcode"></p>
    <p class="text-center text-capitalize"> <b> User address(geo lat) </b>:   <input type=text id="geolet"></p>
    <p class="text-center text-capitalize"> <b> User address(geo lng) </b> :  <input type=text id="geolng"></p>
    <p class="text-center text-capitalize"> <b> User phoneno  *</b> :  <input type=text id="phone"></p>
    <p class="text-center text-capitalize"> <b> User website </b> : <input type=text id="website"></p>
    <p class="text-center text-capitalize"> <b> User Company name  </b> : <input type=text id="companyname"></p>
    <p class="text-center text-capitalize"> <b> User company catchPhrase </b> :<input type=text id="companycatchPhrase"></p>
    <p class="text-center text-capitalize"> <b> User company bs </b>: <input type=text id="companybs"></p>
 
    
    <button id="postApi"   type="submit" value="Submit" class="btn btn-secondary text-capitalize ">POST Details</button>
  
`
    document.getElementById('container').innerHTML = html;

    document.getElementById('postApi').addEventListener('click', postApi);
}


function postApi(e) {
    e.preventDefault();

    let name = document.getElementById('name').value;
    let username = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let street = document.getElementById('street').value;
    let suite = document.getElementById('suite').value;
    let city = document.getElementById('city').value;
    let zipcode = document.getElementById('zipcode').value;
    let geolet = document.getElementById('geolet').value;
    let geolng = document.getElementById('geolng').value;
    let phone = document.getElementById('phone').value;
    let website = document.getElementById('website').value;
    let companyname = document.getElementById('companyname').value;
    let companycatchPhrase = document.getElementById('companycatchPhrase').value;
    let companybs = document.getElementById('companybs').value;
    if (name == "" || username == "" || email == "" || street == "" || city == "" || zipcode == "" || phone == "") {
        alert("fill required fild *");
    } else {
        fetch('https://jsonplaceholder.typicode.com/users', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({ name: name, username: username, email: email, street: street, suite: suite, city: city, zipcode: zipcode, geolet: geolet, geolng: geolng, phone: phone, website: website, companyname: companyname, companycatchPhrase: companycatchPhrase, companybs: companybs })
            })
            .then(function(res) {
                return res.json();
            })
            .then(function(data) {
                let html = '<h2 class="text-center "> NEW USER DETAILS</h2> <hr>';
                html += `  
            <p class="text-center text-capitalize">id : ${data.id} </p> 
            <p class="text-center text-capitalize"><b>Name</b> :${data.name}<p>
            <p class="text-center text-capitalize"><b>User Name</b>:${data.username}</p>
            <p class="text-center text-capitalize"><b>User Email </b>:${data.email}</p>
            <p class="text-center text-capitalize"><b> User address(street) </b> :${data.street}</p>
            <p class="text-center text-capitalize"><b> User address(suite)</b>:${data.suite}</p>
            <p class="text-center text-capitalize"><b> User address(city) </b>:${data.city}</p>
            <p class="text-center text-capitalize"> <b> User address(zipcode) </b>:${data.zipcode}</p>
            <p class="text-center text-capitalize"> <b> User address(geo lat) </b>:${data.geolet}</p>
            <p class="text-center text-capitalize">  <b> User address(geo lng) </b> :${data.geolng}</p>
            <p class="text-center text-capitalize">  <b> User phoneno </b> :${data.phone}</p>
            <p class="text-center text-capitalize">  <b> User website </b>  :${data.website}</p>
            <p class="text-center text-capitalize">  <b> User Company name </b>  :${data.companyname}</p>
            <p class="text-center text-capitalize"> <b> User company catchPhrase </b> :${data.companycatchPhrase}</p>
            <p class="text-center text-capitalize">  <b> User company bs </b> :${data.companybs}</p>
            `

                ;
                // html += ` id:${data.id}`;
                console.log(data)
                console.log(data.name)
                document.getElementById('container').innerHTML = html;
            })
    }


}

function createPutApi() {
    let html = '<h2 class="text-center "> Updated USER DETAILS</h2> <hr>';
    html += `
    <p class="text-center text-capitalize"> <b> Enter id *</b> :  <input type="text" id="id" placeholder="enter id betwwen 1 to 10">
    <p class="text-center text-capitalize"> <b>Name *</b> :  <input type="text" id="name" ></p> 
    <p class="text-center text-capitalize"> <b>User Name *</b>:  <input type=text id="username"></p> 
    <p class="text-center text-capitalize"> <b>User Email *</b>:  <input type=text id="email" ></p>
    <p class="text-center text-capitalize"> <b> User address *(street) </b>:<input type=text id="street"></p>
    <p class="text-center text-capitalize"> <b> User address(suite)</b>: <input type=text id="suite"></p>
    <p class="text-center text-capitalize"> <b> User address(city) * </b>:<input type=text id="city"></p>
    <p class="text-center text-capitalize"> <b> User address(zipcode) * </b>: <input type=text id="zipcode"></p>
    <p class="text-center text-capitalize"> <b> User address(geo lat) </b>:   <input type=text id="geolet"></p>
    <p class="text-center text-capitalize"> <b> User address(geo lng) </b> :  <input type=text id="geolng"></p>
    <p class="text-center text-capitalize"> <b> User phoneno  *</b> :  <input type=text id="phone"></p>
    <p class="text-center text-capitalize"> <b> User website </b> : <input type=text id="website"></p>
    <p class="text-center text-capitalize"> <b> User Company name  </b> : <input type=text id="companyname"></p>
    <p class="text-center text-capitalize"> <b> User company catchPhrase </b> :<input type=text id="companycatchPhrase"></p>
    <p class="text-center text-capitalize"> <b> User company bs </b>: <input type=text id="companybs"></p>
 
    
    <button id="putApi"   type="submit" value="Submit" class="btn btn-secondary text-capitalize ">Update Details</button>
  
`
    document.getElementById('container').innerHTML = html;

    document.getElementById('putApi').addEventListener('click', putApi);
}

function putApi(e, id) {
    e.preventDefault();
    // let ide = document.getElementById('id').value;
    id = document.getElementById('id').value;
    let name = document.getElementById('name').value;
    let username = document.getElementById('username').value;
    let email = document.getElementById('email').value;
    let street = document.getElementById('street').value;
    let suite = document.getElementById('suite').value;
    let city = document.getElementById('city').value;
    let zipcode = document.getElementById('zipcode').value;
    let geolet = document.getElementById('geolet').value;
    let geolng = document.getElementById('geolng').value;
    let phone = document.getElementById('phone').value;
    let website = document.getElementById('website').value;
    let companyname = document.getElementById('companyname').value;
    let companycatchPhrase = document.getElementById('companycatchPhrase').value;
    let companybs = document.getElementById('companybs').value;
    if (name == "" || username == "" || email == "" || street == "" || city == "" || zipcode == "" || phone == "") {
        alert("fill required fild *");
    } else {
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
                method: 'PUT',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({ name: name, username: username, email: email, street: street, suite: suite, city: city, zipcode: zipcode, geolet: geolet, geolng: geolng, phone: phone, website: website, companyname: companyname, companycatchPhrase: companycatchPhrase, companybs: companybs })
            })
            .then(function(res) {
                return res.json();
            })
            .then(function(data) {
                let html = '<h2 class="text-center "> UPDATED USER DETAILS</h2> <hr>';
                html += `  
            <p class="text-center text-capitalize">id : ${data.id} </p> 
            <p class="text-center text-capitalize"><b>Name</b> :${data.name}<p>
            <p class="text-center text-capitalize"><b>User Name</b>:${data.username}</p>
            <p class="text-center text-capitalize"><b>User Email </b>:${data.email}</p>
            <p class="text-center text-capitalize"><b> User address(street) </b> :${data.street}</p>
            <p class="text-center text-capitalize"><b> User address(suite)</b>:${data.suite}</p>
            <p class="text-center text-capitalize"><b> User address(city) </b>:${data.city}</p>
            <p class="text-center text-capitalize"> <b> User address(zipcode) </b>:${data.zipcode}</p>
            <p class="text-center text-capitalize"> <b> User address(geo lat) </b>:${data.geolet}</p>
            <p class="text-center text-capitalize">  <b> User address(geo lng) </b> :${data.geolng}</p>
            <p class="text-center text-capitalize">  <b> User phoneno </b> :${data.phone}</p>
            <p class="text-center text-capitalize">  <b> User website </b>  :${data.website}</p>
            <p class="text-center text-capitalize">  <b> User Company name </b>  :${data.companyname}</p>
            <p class="text-center text-capitalize"> <b> User company catchPhrase </b> :${data.companycatchPhrase}</p>
            <p class="text-center text-capitalize">  <b> User company bs </b> :${data.companybs}</p>
            `;

                console.log(data)
                console.log(data.name)
                document.getElementById('container').innerHTML = html;
            })
    }

}

function createDeleteApi() {
    let html = '';
    html += ` <p class="text-center text-capitalize"> <b> Enter id *</b> :  <input type="text" id="id" placeholder="enter id betwwen 1 to 10"></p>
    <button id="deleteApi"   class="btn btn-secondary text-capitalize "> Delete Details</button> `
    document.getElementById('container').innerHTML = html;
    document.getElementById('deleteApi').addEventListener('click', deleteApi);
}




function deleteApi(e, id) {
    e.preventDefault();
    id = document.getElementById('id').value;
    if (id == "") {
        alert("fill required fild *");
    } else {
        fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
                method: 'DELETE',
            })
            .then(function(res) {
                return res.json();
            })
            .then(function(data) {
                console.log(data)
            })
        alert(`data sucessfully deleted id no ${id} `)
    }

}